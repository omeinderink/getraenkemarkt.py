import sys

class Getraenk:
    def __init__(self, nummer, name, vorrat):
        self.nummer = nummer
        self.name = name 
        self.vorrat = vorrat

    def beschreibung(self):
        beschr_str = "Es sind %d Einheiten von Getränk Nr. %d mit der Bezeichnung %s vorhanden." % (self.vorrat, self.nummer, self.name)
        return beschr_str

class Transaktion:
    def eingabe(self, auswahl, menge):
        while auswahl not in range(1,4):
            for i in list(getraenke.keys()):
                print(getraenke[i].nummer,": ",getraenke[i].name)
            try:
                auswahl = int(input("Geben Sie das Getränk an: "))
            except ValueError:
                pass
        
        while menge not in range(1,getraenke[auswahl].vorrat + 1):
            print(getraenke[auswahl].vorrat," Einheiten vorhanden.")
            try:
                menge = int(input("Geben Sie die Menge an :"))
            except ValueError:
                pass

        if getraenke[auswahl].vorrat > 0 and getraenke[auswahl].vorrat >= menge:
            getraenke[auswahl].vorrat -= menge

        if menge == 1:
            input_str = "%d Einheit %s" %(menge, getraenke[auswahl].name)
        else:
             input_str = "%d Einheiten %s" %(menge, getraenke[auswahl].name)
        return input_str




cola = Getraenk(1,"Coca-Cola",100)
selter = Getraenk(2,"Fürst Bismarck",500)
schweppes = Getraenk(3,"Ginger Ale",87)

getraenke = {
    1 : cola,
    2 : selter,
    3 : schweppes
}



run = 0
while True:

    for i in list(getraenke.keys()):
        print(getraenke[i].beschreibung())

    modus = 0
    while modus not in range(1,3):
        try:
            modus = int(input("Bitte wählen: Transaktion(1) oder Beenden(2)"))
        except ValueError:
            pass
    if modus == 1:
        ta = Transaktion()
        print(ta.eingabe(0,0))
    if modus == 2:
        sys.exit()

    run += 1
    print("Durchläufe: %d" %(run))
